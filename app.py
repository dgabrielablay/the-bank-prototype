from flask import *
import os
import pymongo
from bson.objectid import ObjectId
from bson.binary import Binary
from bson import BSON
import datetime
from werkzeug.utils import secure_filename
from PIL import Image
from io import BytesIO
import math

DB_NAME = 'heroku_v07c1j2z'  
DB_HOST = 'ds231133.mlab.com'
DB_PORT = 31133
DB_USER = 'admin' 
DB_PASS = 'password1'

myclient = pymongo.MongoClient(DB_HOST, DB_PORT)
mydb = myclient[DB_NAME]
mydb.authenticate(DB_USER, DB_PASS)
myusers = mydb['users']
mycompanies = mydb['companies']
myadmins = mydb['admins']
mylogs = mydb['logs']

UPLOAD_FOLDER = 'static/images'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']

app = Flask(__name__)
app.secret_key = 'bank-secret-key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule(
	'/assets/img',
	endpoint='assets',
	view_func=app.send_static_file
)


@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
	if endpoint == 'static':
		filename = values.get('filename', None)
		if filename:
			file_path = os.path.join(app.root_path,
									endpoint, filename)
			values['q'] = int(os.stat(file_path).st_mtime)
	return url_for(endpoint, **values)

## 
# PAGES
##

##
# LANDING PAGE
##
@app.route('/')
def landing():
	total_transactions = len([log for log in mylogs.find()])
	total_users = len([user for user in myusers.find()])
	total_blocks = math.ceil(total_transactions/20)
	total_companies = len([company for company in mycompanies.find()])
	safe_percentage = 100
	return render_template(
		'index.html',
		total_transactions=total_transactions,
		total_users= total_users,
		total_blocks=total_blocks,
		safe_percentage=safe_percentage,
		total_companies=total_companies
	)

##
# LOGOUT
##
@app.route('/logout')
def logout():
	url = ('user_login', 'admin_login')[is_admin()]
	session.pop('email', None)
	session.pop('type', None)
	return redirect(url_for(url))

##
# ADD USER
##
@app.route('/user/add/submit', methods=['POST', 'GET'])
def user_add_submit():
	if request.method == 'POST':
		user = {
			'name': request.form['name'],
			'email': request.form['email'],
			'password': request.form['password'],
			'money': 0,
			'current': 0,
			'pin': '0000',
			'timestamp': current_timestamp()
		}
		myusers.insert_one(user)
		mylogs.insert_one({
			'email': user['email'],
			'category': 'account',
			'timestamp': current_timestamp()
		})
		return redirect(url_for('user_login'))
	return redirect(url_for('landing'))


##
# ADMIN - PAGES
##

##
# ADMIN - LOGIN PAGE
##
@app.route('/admin/login')
def admin_login():
	if is_admin():
		return redirect(url_for('admin_admins'))
	if is_user():
		return redirect(url_for('user_home'))
	return render_template(
		'admin/login.html'
	)

##
# ADMIN - LOGIN SUBMIT
##
@app.route('/admin/logout/submit', methods=['POST', 'GET'])
def admin_login_submit():
	if request.method == 'POST':
		admin = {
			'email': request.form['email'],
			'password': request.form['password']
		}
		try:
			x = list(myadmins.find_one(admin))
			admin = myadmins.find_one(admin)
			session['email'] = admin['email']
			session['type'] = 'admin'
			return redirect(url_for('admin_admins'))
		except:
			return redirect(url_for('admin_login'))

##
# ADMIN - ADMINS LIST
##
@app.route('/admin/admin_list')
def admin_admins():
	if not is_admin():
		return redirect(url_for('admin_login'))
	return render_template(
		'admin/admins.html',
		admins=myadmins.find()
	)

##
# ADMIN - NEW ADMIN PAGE
##
@app.route('/admin/new_admin')
def admin_new_admin():
	if not is_admin():
		return redirect(url_for('admin_login'))
	return render_template(
		'admin/new_admin.html'
	)

##
# ADMIN - NEW ADMIN SUBMIT
##
@app.route('/admin/new_admin/submit', methods=['POST', 'GET'])
def admin_new_admin_submit():
	if request.method == 'POST':
		admin = {
			'name': request.form['name'],
			'email': request.form['email'],
			'password': request.form['password'],
			'timestamp': current_timestamp()
		}
		myadmins.insert_one(admin)
		return redirect(url_for('admin_admins'))

##
# ADMIN - DELETE ADMIN
##
@app.route('/admin/admin_list/<admin_id>/delete')
def admin_delete_admin(admin_id):
	if not is_admin():
		return redirect(url_for('admin_login'))
	myadmins.delete_one({'_id': ObjectId(admin_id)})
	return redirect(url_for('admin_admins'))

##
# ADMIN - COMPANIES LIST
##
@app.route('/admin/companies_list')
def admin_companies():
	if not is_admin():
		return redirect(url_for('admin_login'))
	return render_template(
		'admin/companies.html',
		companies=mycompanies.find()
	)

##
# ADMIN - NEW COMPANY PAGE
##
@app.route('/admin/new_company')
def admin_new_company():
	if not is_admin():
		return redirect(url_for('admin_login'))
	return render_template(
		'admin/new_company.html'
	)

##
# ADMIN - NEW COMPANY SUBMIT
##
@app.route('/admin/new_company/submit', methods=['POST', 'GET'])
def admin_new_company_submit():
	if request.method == 'POST':
		company = {
			'name': request.form['name'],
			'email': request.form['email'],
			'password': request.form['password'],
			'timestamp': current_timestamp()
		}
		mycompanies.insert_one(company)
		return redirect(url_for('admin_companies'))

##
# ADMIN - DELETE COMPANY
##
@app.route('/admin/companies_list/<company_id>/delete')
def admin_delete_company(company_id):
	if not is_admin():
		return redirect(url_for('admin_login'))
	mycompanies.delete_one({'_id': ObjectId(company_id)})
	return redirect(url_for('admin_companies'))

##
# ADMIN - USERS
##
@app.route('/admin/users_list')
def admin_users_list():
	if not is_admin():
		return redirect(url_for('admin_login'))
	length = len([user for user in myusers.find()])
	return render_template(
		'admin/users.html',
		length=length,
		users=myusers.find()
	)

##
# ADMIN - DELETE USER
##
@app.route('/admin/users_list/<user_id>/delete')
def admin_delete_user(user_id):
	if not is_admin():
		return redirect(url_for('admin_login'))
	myusers.delete_one({'_id': ObjectId(user_id)})
	return redirect(url_for('admin_users_list'))

##
# ADMIN - HOME PAGE
##
@app.route('/admin/users_list/<email>')
def admin_home(email):
	if not is_admin():
		return redirect(url_for('admin_login'))
	user = myusers.find_one({'email': email})
	timeline = mylogs.find({'email': email}).sort('timestamp', -1)
	categorized_timeline = categorize_timeline(timeline)
	return render_template(
		'admin/home.html',
		user=user,
		commarize=commarize,
		timeline=categorized_timeline
	)

##
# ADMIN - TRANSACTIONS
##
@app.route('/admin/transactions')
def admin_transactions():
	if not is_admin():
		return redirect(url_for('admin_login'))
	
	timeline = mylogs.find().sort('timestamp', -1)
	categorized_timeline = categorize_timeline(timeline)
	return render_template(
		'admin/transactions.html',
		timeline=categorized_timeline,
		commarize=commarize
	)

##
# USER - PAGES
##

##
# USER - LOGIN
##
@app.route('/user/login')
def user_login():
	if is_user():
		return redirect(url_for('user_home'))
	if is_admin():
		return redirect(url_for('admin_admins'))
	return render_template('user/login.html')

##
# USER - LOGIN SUBMIT
##
@app.route('/user/logout/submit', methods=['POST', 'GET'])
def user_login_submit():
	if request.method == 'POST':
		user = {
			'email': request.form['email'],
			'password': request.form['password']
		}
		try:
			x = list(myusers.find_one(user))
			user = myusers.find_one(user)
			session['email'] = user['email']
			session['type'] = 'user'
			return redirect(url_for('user_home'))
		except:
			return redirect(url_for('user_login'))

##
# USER - HOME PAGE
##
@app.route('/user/home')
def user_home():
	if not is_user():
		return redirect(url_for('user_login'))
	user = myusers.find_one({'email': session['email']})
	timeline = mylogs.find({'email': session['email']}).sort('timestamp', -1)
	categorized_timeline = categorize_timeline(timeline)
	companies = mycompanies.find({}, {
		'_id': 1,
		'name': 1
	})
	return render_template(
		'user/home.html',
		user=user,
		commarize=commarize,
		companies=companies,
		timeline=categorized_timeline
	)

##
# USER - CURRENT DEPOSIT
##
@app.route('/user/deposit/current/submit', methods=['POST', 'GET'])
def user_current_deposit():
	if request.method == 'POST':
		amount = int(request.form['amount_deposit'])
		user = myusers.find_one({'email': session['email']})
		mylogs.insert_one({
			'email': user['email'],
			'amount': amount,
			'category': 'deposit',
			'timestamp': current_timestamp(),
			'type': 'current'
		})
		myusers.update_one(
			{'_id': ObjectId(user['_id'])},
			{'$set': {
				'current': user['current'] + amount
			}}
		)
		flash('Successful transaction.', 'success')
	return redirect(url_for('user_home'))

##
# USER - CURRENT WITHDRAW
##
@app.route('/user/withdraw/current/submit', methods=['POST', 'GET'])
def user_current_withdraw():
	if request.method == 'POST':
		amount = int(request.form['amount_withdraw'])
		if amount%100 != 0:
			flash('Amount to withdraw should be a multiple of 100s only.', 'error')
			return redirect(url_for('user_home'))
		user = myusers.find_one({'email': session['email']})
		mylogs.insert_one({
			'email': user['email'],
			'amount': amount,
			'category': 'withdraw',
			'timestamp': current_timestamp(),
			'type': 'current'
		})
		myusers.update_one(
			{'_id': ObjectId(user['_id'])},
			{'$set': {
				'current': user['current'] - amount
			}}
		)
		flash('Successful transaction.', 'success')
	return redirect(url_for('user_home'))

##
# USER - SAVINGS DEPOSIT
##
@app.route('/user/deposit/submit', methods=['POST', 'GET'])
def user_deposit():
	if request.method == 'POST':
		amount = int(request.form['amount_deposit'])
		user = myusers.find_one({'email': session['email']})
		mylogs.insert_one({
			'email': user['email'],
			'amount': amount,
			'category': 'deposit',
			'timestamp': current_timestamp()
		})
		myusers.update_one(
			{'_id': ObjectId(user['_id'])},
			{'$set': {
				'money': user['money'] + amount
			}}
		)
		flash('Successful transaction.', 'success')
	return redirect(url_for('user_home'))

##
# USER - SAVINGS WITHDRAW
##
@app.route('/user/withdraw/submit', methods=['POST', 'GET'])
def user_withdraw():
	if request.method == 'POST':
		amount = int(request.form['amount_withdraw'])
		if amount%100 != 0:
			flash('Amount to withdraw should be a multiple of 100s only.', 'error')
			return redirect(url_for('user_home'))
		user = myusers.find_one({'email': session['email']})
		mylogs.insert_one({
			'email': user['email'],
			'amount': amount,
			'category': 'withdraw',
			'timestamp': current_timestamp()
		})
		myusers.update_one(
			{'_id': ObjectId(user['_id'])},
			{'$set': {
				'money': user['money'] - amount
			}}
		)
		flash('Successful transaction.', 'success')
	return redirect(url_for('user_home'))

##
# USER - TRANSFER MONEY
##
@app.route('/user/transfer/submit', methods=['POST', 'GET'])
def user_transfer():
	if request.method == 'POST':
		try:
			user_id = ObjectId(request.form['user_id'])
			amount = int(request.form['amount_transfer'])
			user = myusers.find_one({'email': session['email']})
			reciever = myusers.find_one({'_id': user_id})
			x = list(reciever)
			mylogs.insert_one({
				'email': user['email'],
				'to': reciever['email'],
				'amount': amount,
				'category': 'transfer',
				'timestamp': current_timestamp(),
				'type': 'savings'
			})
			mylogs.insert_one({
				'email': reciever['email'],
				'from': user['email'],
				'amount': amount,
				'category': 'recieve',
				'timestamp': current_timestamp(),
				'type': 'savings'
			})
			myusers.update_one(
				{'_id': ObjectId(user['_id'])},
				{'$set': {
					'money': user['money'] - amount
				}}
			)
			myusers.update_one(
				{'_id': user_id},
				{'$set': {
					'money': user['money'] + amount
				}}
			)
			flash('Successful transaction.', 'success')
		except:
			""
	return redirect(url_for('user_home'))

##
# USER - PAY BILLS
##
@app.route('/user/bills/pay', methods=['POST', 'GET'])
def user_pay_bill():
	if request.method == 'POST':
		try:
			company_id = ObjectId(request.form['company_id'])
			amount = int(request.form['amount_transfer'])
			user = myusers.find_one({'email': session['email']})
			reciever = mycompanies.find_one({'_id': company_id})
			x = list(reciever)
			mylogs.insert_one({
				'email': user['email'],
				'to': reciever['name'],
				'amount': amount,
				'category': 'transfer',
				'timestamp': current_timestamp(),
				'type': 'current'
			})
			mylogs.insert_one({
				'email': reciever['email'],
				'from': user['email'],
				'amount': amount,
				'category': 'recieve',
				'timestamp': current_timestamp(),
				'type': 'current'
			})
			myusers.update_one(
				{'_id': ObjectId(user['_id'])},
				{'$set': {
					'current': user['current'] - amount
				}}
			)
			mycompanies.update_one(
				{'_id': company_id},
				{'$set': {
					'current': user['current'] + amount
				}}
			)
			user = myusers.find_one({'email': session['email']})
			if user['current'] < 0:
				fee = user['current'] * TRANSFER_DEDUCTION_RATE
				mylogs.insert_one({
					'email': user['email'],
					'to': 'BANKLANG TWO',
					'amount': fee,
					'category': 'transfer fee',
					'timestamp': current_timestamp(),
					'type': 'current'
				})
				myusers.update_one(
					{'_id': ObjectId(user['_id'])},
					{'$set': {
						'current': user['current'] - fee
					}}
				)
			flash('Successful transaction.', 'success')
		except Exception as e:
			print(e)
	return redirect(url_for('user_home'))

##
# USER - CHANGE PIN
##
@app.route('/user/pin/change', methods=['POST', 'GET'])
def user_pin_change():
	if request.method == 'POST':
		old_pin = request.form['old_pin']
		new_pin = request.form['new_pin']
		confirm_pin = request.form['confirm_pin']
		print(old_pin, new_pin, confirm_pin)
		user = myusers.find_one({'email': session['email']})
		if user['pin'] != old_pin:
			flash('Invalid previous PIN.', 'error')
			return redirect(url_for('user_home'))
		if new_pin != confirm_pin:
			flash('New PIN is not equal to the confirmation PIN.', 'error')
			return redirect(url_for('user_home'))
		myusers.update_one(
			{'_id': user['_id']},
			{'$set': {
				'pin': new_pin
			}}
		)
		flash('Successful PIN change.', 'success')
	return redirect(url_for('user_home'))

##
# USER - REQUEST PIN
##
@app.route('/user/pin/request/<pin>')
def user_request_pin(pin):
	user = myusers.find_one({'email': session['email']})
	if user['pin'] != pin:
		return Response(json.dumps({
			'correct': False,
			'value': 'Invalid PIN'
		}), mimetype='application/json')
	return Response(json.dumps({
		'correct': True,
		'value': 'shit'
	}), mimetype='application/json')

##
# UTILITIES
##

TRANSFER_DEDUCTION_RATE = -0.01

def is_session_active():
	return (False, True)['type' in session]

def is_admin():
	return (False, True)[is_session_active() and session['type'] == 'admin']

def is_user():
	return (False, True)[is_session_active() and session['type'] == 'user']

def current_timestamp():
	return datetime.datetime.now()

def commarize(number):
    return '{:,.2f}'.format(float(number))

def get_readable_date(date):
    return '{:%B %d, %Y}'.format(date)

def categorize_timeline(timeline):
	categorized_timeline = {}
	for activity in timeline:
		date = get_readable_date(activity['timestamp'])
		if date not in categorized_timeline:
			categorized_timeline[get_readable_date(activity['timestamp'])] = [
				activity
			]
		else:
			categorized_timeline[date].append(activity)
	return categorized_timeline

if __name__ == "__main__":
	app.run('0.0.0.0', debug=True, port=5001)