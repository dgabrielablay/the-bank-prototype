class Transaction():
	def __init__(self, username_from, username_to, amount):
		self.username_from = username_from
		self.username_to = username_to
		self.amount = amount

	def to_json(self):
		return str({
			'from': self.username_from,
			'to': self.username_to,
			'amount': self.amount
		})