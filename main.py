from classes.Block import Block
from classes.Transaction import Transaction

def main():
	blockchain = []
	genesis_hash = 0
	genesis_transactions = [Transaction('don', 'don', 0)]
	genesis_block = Block(
		genesis_hash,
		genesis_transactions
	)
	blockchain.append(genesis_block)
	block =  Block(
		blockchain[len(blockchain)-1]._id,
		[Transaction('don', 'don', 1)]
	)
	blockchain.append(block)
	block =  Block(
		blockchain[len(blockchain)-1]._id,
		[Transaction('don', 'don', 2),
		 Transaction('don', 'hazel', 3)]
	)
	blockchain.append(block)
	[print(block._id) for block in blockchain]

if __name__ == "__main__":
	main()
