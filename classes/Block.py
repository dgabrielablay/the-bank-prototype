import json
import classes.make_json_serializable

class Block:
	def __init__(self, previous_hash, transactions):
		self.previous_hash = previous_hash
		self.transactions = transactions
		content = json.dumps({
			'previous_hash': previous_hash,
			'transactions': transactions
		})
		self._id = hash(content)